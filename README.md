# OSCAR - Open Source Supply Chains and Avoidance of Risk: An evidence based approach to improve FLOSS supply chains

# Objective 

The primary aim is to develop theoretical, computational, and statistical frameworks that
discover, collect, and process FLOSS operational data to construct FLOSS SC, to identify and
quantify its risks, and to discover and construct effective risk mitigation practices and tools.

## Overall description of the data collection and analysis methods

This describes the data collection/update https://bitbucket.org/swsc/overview/raw/master/pubs/WoC.pdf

[A tutorial on how to use WoC](https://bitbucket.org/swsc/lookup/src/master/tutorial.md)

[WoC APIs](https://bitbucket.org/swsc/lookup/src/master/README.md)


The breakdown into microservices that coontain three major pieces:

     - https://github.com/ssc-oscar/gather is for discovery of new/updated repos
     - https://github.com/ssc-oscar/libgit2 is for efficiently getting only necessary git objects from remotes 
     - https://bitbucket.org/swsc/lookup/src/master/ thats where updates, mapping to analytic layers, and scripts to query stuff are. Still needs refactoring into update/query parts
     - https://github.com/ssc-oscar/oscar.py is for analytics APIs to query the data
     

* Sections
    - [Major language usage and productivity trends](https://bitbucket.org/swsc/overview/src/master/usage/README.md)
    - [Fun facts about WoC Collection](https://bitbucket.org/swsc/overview/src/master/fun/README.md)
    - [The most popular components -- used in most projects](https://bitbucket.org/swsc/overview/src/master/deps/README.md)

* Talks
    - [FSE'19 Keynote](https://bitbucket.org/swsc/overview/raw/master/pubs/WoCFse19.pdf)
    - [Talk at Shenzhen Pengcheng Lab workshop on open source big data](https://bitbucket.org/swsc/overview/raw/master/pubs/WoCShenZhen19.pdf)
    - [Software Supply Chain'2017](https://bitbucket.org/swsc/overview/raw/master/pubs/PKU2017Keynote.pdf)
    - [Software Supply Chain'2018](https://bitbucket.org/swsc/overview/raw/master/pubs/PKU2018Keynote.pdf)
    - [EHR supply chain](https://bitbucket.org/swsc/overview/raw/master/pubs/HICSS2019_EHRpanel.pdf) 
    - [EHR supply chain](https://bitbucket.org/swsc/overview/raw/master/pubs/HICSS2019_EHRpanel.pdf) 

* Reports
    - [Developer Reputation Estimator (DRE): Initial Submission - Sadika Amreen, Andrey Karnauch, and Audris Mockus](https://bitbucket.org/swsc/overview/raw/master/pubs/ase-tools-paper.pdf)
    - [FLOSS Census: World of Code: Early draft: Yuxing Ma, Chris Bogart, Sadika Amreen, Russell Zaretzki, and Audris Mockus. World of code: An infrastructure for mining the universe of open source vcs data. MSR'19.](https://bitbucket.org/swsc/overview/raw/master/pubs/WoC.pdf)
    - [How to measure software supply chain? Sadika Amreen, Bogdan Bichescu, Randy Bradley, Tapajit Dey, Yuxing Ma, Audris Mockus, Sara Mousavi, and Russell Zaretzki. A methodology for measuring floss ecosystems. In Towards Engineering Free/Libre Open Source Software (FLOSS) Ecosystems for Impact and Sustainability: Communications of NII Shonan Meetings. 2019](https://bitbucket.org/swsc/overview/raw/master/pubs/measuring-supply-chain.pdf)
    - [Why technology gets popular?](https://bitbucket.org/swsc/overview/raw/master/pubs/methodology-analyzing-uptake.pdf)
    - [How to get valid social networks?](https://bitbucket.org/swsc/overview/raw/master/pubs/correcting-developer-identity.pdf)

* Blogs
    - [Coke vs. Pepsi? data.table vs. tidy? Examining Consumption Preferences for Data Scientists](https://www.r-bloggers.com/coke-vs-pepsi-data-table-vs-tidy-examining-consumption-preferences-for-data-scientists/)
    - [Coke vs. Pepsi? data.table vs. tidy? Part 2](https://r-posts.com/coke-vs-pepsi-data-table-vs-tidy-part-2)
    
## Status

This data (version Q) collected on Nov 9 based on updates/new repositories identified on Oct 15, 2019, 
and the git objects have been retrieved by Dec 28. 

The number of commits/trees/tags, projects (distinct repositories), and 
distinct author IDs are shown in the table below. 
The notable increase in the number of projects from version P is mainly due to collecting 
commits from all the forks. The number of unique projects (projects that share no commits) is much lower: 61921909. 
The largest group of forked (according to the relationship of a shared commit) projects contains 13912612 distinct repositories.
Only nonempty repositories are included in these counts. 
Furthermore, only 65569914 disctinct clusters of unrelated (through parent/child relationship) 
commits exist and of these only 51918448 groups contain more than one commit. The largest group has
53709923 commits related via parent-child relationship.


Number|type
------|----
7204111388|blob
1868632121|commit
7704499885|tree
16172556|tag
116265607|projects (distinct repositories)
38142898|author IDs



This data (version P)  collection on May 15-June 5 based on updates/new repositories identified on May 15, 2019
has 73314320 unique non-forked git repositories, 34424362 unique author IDs, and

Number|type
------|----
6466663836|blob
1685985529|commit
6861187752|tree
12794392|tag

This data (version O)  collection on Apr 1-10 based on updates/new repositories identified on Apr 10, 2019
has 68386801 unique non-forked git repositories, 32757289 unique author IDs, and

Number|type
------|----
6028240353|blob
1592136050|commit
6477758655|tree
12343516|tag


This data (version N)  collection on Feb 15-25 based on updates/new repositories identified on Feb 10, 2019, had
31356272 unique author IDs,  65679542 unique non-forked git repositories and

Number|type
------|----
5761659437|blob
1524390485|commit
6209360212|tree
11712941|tag



Version M collection on Jan 10-20 based on updates/new repositories identified on Jan 3, 2019, had

Number|type
------|----
5561137754| blob (considered by git to be text files)
1468800973| commit
6009633262| tree
11470696|tag

This data version (M) has the total number of distinct repos at 63847092 (48005716 adjusted for
forking, i.e., sharing a commit) and the number of unique author IDs at 30439015. 


Collection on Dec 10-22 based on updates/new repositories identified on Dec 3, 2018, had

Number|type
------|----
5313256585| blob (considered by git to be text files)
1419161099| commit
5786313329| tree
9518401 |tag

This data version is version L


Furthermore, 28455871 unique author strings were in the 1.4B commits of data version K.
This number is raw and not adjusted for synonyms and is based on the actual changes to the 
code, not reporting issues. The number of code authors that had more than 25 commits is 
approximately 20% of the total (5.7M) and 10% (or 2.8M) have made more than 63 commits.
7M have made just one commit. 

The number of unique projects (repos) is 59627553. 


21935945 unique author strings were in the 1.1B commits of data version J.

## Team

Name|Email|Role
----|-----|----
Audris Mockus|audris@utk.edu| PD/PI
James Herbsleb| jdh@cs.cmu.edu | Co-PD/PI
Bogdan C Bichescu| bbichescu@utk.edu|Co-PD/PI
Randy Bradley | rbradley@utk.edu| Co-PD/PI
Russell L Zaretzki| rzaretzk@utk.edu|Co-PD/PI
Chris Bogart | cbogart@cmu.edu | Co-PD (Research Scientist)
Minghui Zhou | zhmh@pku.edu.cn | Co-PD (Professor)
Sadika Amreen| samreen@vols.utk.edu|Graduate Student (research assistant)
Tapajit Dey| tdey2@vols.utk.edu| Graduate Student (research assistant)
Kai Gao | gaokaipku@qq.com | Undegraduate Student (research assistant)
Andrey Karnauch| akarnauc@vols.utk.edu| Graduate Student (research assistant)
Yuxing Ma|yma28@vols.utk.edu|Graduate Student (research assistant)
Martha E. Milhollin| mmilholl@vols.utk.edu|Graduate Student (research assistant , Jan-Apr 2019)
Sara Mousavi| mousavi@vols.utk.edu| Graduate Student (research assistant)
Zachary Trzil | ztrzil@vols.utk.edu | Undergraduate Student (research assistant, Feb-March 2018)
Marat Valiev | marat@cmu.edu | Graduate Student (research assistant)
YuLin Xu | xyl_xuyulin@126.com | Undegraduate Student (research assistant)
Yuxia Zhang | yuxiaz@pku.edu.cn | Graduate Student (research assistant)

# High level description

Open source software is an engine for innovation and a critical
infrastructure for the nation and yet it is implemented by
communities formed from a loose collection of individuals. With each
software project relying on thousands of other software projects,
this complex and dynamic supply chain introduces new risks and
unpredictability, since, unlike in traditional software projects, no
contractual relationships with the community exist and individuals
could simply lose interest or move on to other activities. This
project aims to advance the state of knowledge of software supply
chains by collecting and integrating massive public operational data
representing development activity and source code from all open
source projects and using it to develop novel theories, methods, and
tools. The transformative change in the theoretical basis for the
supply chains from data-poor, survey-based to data-rich operational
data-based approaches will lead to development of tools and
practices to quantify and mitigate previously unknown risks in the
rapidly changing global environment with no centralized control or
authority. Expected results include dramatic reductions in risks
manifested in, for example, the spread of vulnerabilities or
knowledge loss, making the nation both safer and more innovative. The
novel theoretical frameworks are expected to provide basis for
emerging supply chains, such as 3-D blueprints.  The
outreach to open source communities and supporting companies is
expected to transform the way open source software is selected and
developed. The workshops and courses will train a new generation of
scientists and software engineers capable of quantifying and
managing risks in decentralized environments.

The project will construct and analyze the open source supply chain
(OSSC) to determine its global and local properties.  The properties
of individual nodes will be modeled using software development
activity data and integrated with the OSSC network to identify the
dynamic properties of the network, risk propagation, and
system-level risks. Statistical and game-theoretic models will be
used to evaluate processes with the goal of assessing and mitigating
these risks. Methods to contextualize, augment, and correct
operational data will be developed to cope with data's size,
complexity, and observational nature.  In an OSSC the end users and
downstream developers have an ongoing need to ensure continued
compatibility and to respond in a coordinated way to changes in
operating systems, libraries, hardware, and other
infrastructure, but practical or theoretical guidance on how to manage
risks and do it effectively is lacking with many risks either
not recognized or not evaluated in OSSCs.  Version control systems
record code changes along with metadata including author, date, and
commit description explaining the reason for the change. This
operational data provides basic building blocks linking developers
and code and is used in four research themes: 1) identify
visibility, network, and congruency risks of OSSC, 2) develop ways
of evaluating these risks, 3) identify and develop practices to
mitigate these risks, and 4) develop computational models and tools
to assess and mitigate risks for specific value chains.



## Acknowledgment
This material is based upon work supported by the National Science Foundation under Grant No. 1633437

## Disclaimer
Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation.
