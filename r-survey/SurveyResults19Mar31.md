---
title: "Survey Results"
date: "Updated March 31, 2019"
output:
  html_document: default
  #word_document: default
---

```{r setup, eval = T, include = F}
knitr::opts_chunk$set(echo = TRUE, warning=F, message=F)
(.packages())

library(rlang)
library(data.table)
library(tidyverse)
library(lubridate)
library(tm)
library(SnowballC)
library(wordcloud)
library(RColorBrewer)
library(plyr)
library(AUC)
library(randomForest)
library(corrplot)
library(varhandle)

make_cloud <- function(words){
  text <- words[words != ""]
  docs <- Corpus(VectorSource(text))
  #inspect(docs)
  
  toSpace <- content_transformer(function (x , pattern ) gsub(pattern, " ", x))
  docs <- tm_map(docs, toSpace, "/")
  docs <- tm_map(docs, toSpace, "@")
  docs <- tm_map(docs, toSpace, "\\|")
  
  # Convert the text to lower case
  docs <- tm_map(docs, content_transformer(tolower))
  # Remove numbers
  docs <- tm_map(docs, removeNumbers)
  # Remove english common stopwords
  docs <- tm_map(docs, removeWords, stopwords("english"))
  # Remove punctuations
  docs <- tm_map(docs, removePunctuation)
  # Eliminate extra white spaces
  docs <- tm_map(docs, stripWhitespace)
  # Text stemming
  #docs <- tm_map(docs, stemDocument)
  
  dtm <- TermDocumentMatrix(docs)
  m <- as.matrix(dtm)
  v <- sort(rowSums(m),decreasing=TRUE)
  d <- data.frame(word = names(v),freq=v)
  head(d, 10)
  
  set.seed(1234)
  wordcloud(words = d$word, freq = d$freq, min.freq = 1,
            max.words=200, random.order=FALSE, rot.per=0.35, 
            colors=brewer.pal(8, "Dark2"))
}

survey <- fread("SurveyResults19Mar31.csv")
names(survey) <- tolower(names(survey))
survey <- survey[package %in% c("data.table", "tidy")] #one row is missing this
```

## Analysis of Tech Survey Results

This document explores the results of the technical survey of data.table and tidy users.

Note that if a question was completed (submitted), that question was included in the analysis even if the entire survey was not completed.  There were 12 questions in the survey.

```{r}
dim(survey)
table(survey$completed) # 803/1085 = 74% of respondents completed all 12 questions
table(survey$device, survey$completed) #more likely to complete on PC
table(survey$package) #intial data set breakdown by package
```

```{r, eval = T, include = F}
#begin to calculate duration of survey
typeof(survey$start)
typeof(survey$end)
head(survey$start, 10)

#convert character to date/time
survey$start <- ymd_hms(survey$start)
survey$end <- ymd_hms(survey$end)

#add column for duration of time to complete survey
survey[,duration := end - start]
#type is difftime
survey$duration <- as.double(survey$duration)
range(survey$duration, na.rm = T) #seconds
quantile(survey$duration, probs = seq(0, 1, 0.1), na.rm = T)

#consider duration by device
by_device <- survey[,.(seconds = quantile(duration, probs = seq(0, 1, 0.1), na.rm = T)), by = device]
by_device$quantile <- seq(0,1,.1)
```

### How long did it take to complete 12-question survey?

```{r, echo=F, warning=F}
#remove last decile and plot without extremes
by_device %>%
  filter(device %in% c("PC", "Phone", "Tablet")) %>%
  filter(quantile < 1) %>%
  ggplot(aes(x = quantile, y = seconds, color = device)) +
    geom_point() +
    geom_line()

#Duration less than 10 minutes
survey %>%
  filter(duration < 600) %>%
  ggplot(aes(x = duration)) +
  geom_histogram(fill = "darkturquoise") +
  geom_vline(aes(xintercept = mean(duration)), color = "black") +
  geom_vline(aes(xintercept = median(duration)), color = "black")
#Mean and Median duration in seconds
survey %>% 
  filter(duration < 600) %>%
  summarize(mean = mean(duration, na.rm=T),median = median(duration, na.rm=T))
```

### What year was commit executed?
### Note tidy did not become a choice until late 2014.

```{r, echo=F}
survey %>%
  dplyr::group_by(commit_year, package) %>%
  dplyr::summarize(count = n()) %>%
  ggplot(aes(x=commit_year, y=count, group=package, fill = package)) +
  geom_bar(stat="identity", position=position_dodge2(preserve="single")) +
  geom_text(aes(label=count, hjust=0.5, vjust=0),
            position = position_dodge(width = 1))  +
  theme(axis.text.x = element_text(angle = 30)) +
  theme(legend.position = "bottom") +
  labs(title = "Commits by Year",x = "Year", y="Count") +
  theme(plot.title = element_text(hjust = 0.5))
```

### Question 1 - What was the purpose of this project?

Four options were provided, and the respondent could select as few or as many as desired.  The options included:

1. For personal work and/or research use
2. For a wider audience, such as developers of other packages or other software
3. For a training / class that I took
4. Other (respondent could provide additional information here)

```{r, eval=T, include=F}
pg1_answers <- c("For personal work and/or research use",
                 "For a wider audience, such as developers of other packages or other software",
                 "For a training / class that I took",
                 "Other",
                 "")
#pg1psnuse column also contains other responses
#expected it to contain only 1
unique(survey$pg1psnuse)

#set new variable pg1psnuse_cat to 1 if "For personal work and/or research use" exists
#used as categorical variable in later analysis
survey[,pg1psnuse_cat := 0]
survey[pg1psnuse == pg1_answers[1], pg1psnuse_cat := 1]
table(survey$pg1psnuse_cat)
#capture when question 1 was not completed
survey[pg1submit == "", pg1psnuse_cat := NA]
table(survey$pg1psnuse_cat)

#wrangle all the comments into new other "column"
unique(survey$pg1other)
survey[!(pg1psnuse %in% pg1_answers), pg1other := pg1psnuse]

#set new variable pg1wdauth_cat to 1 if exists
unique(survey$pg1wdauth)
survey[,pg1wdauth_cat := 0]
survey[pg1wdauth == pg1_answers[2], pg1wdauth_cat := 1]
table(survey$pg1wdauth_cat)
#capture when question 1 was not completed
survey[pg1submit == "", pg1wdauth_cat := NA]
table(survey$pg1wdauth_cat)

#wrangle all the comments into new "other" column
unique(survey$pg1other)
survey[!(pg1wdauth %in% pg1_answers), pg1other := pg1wdauth]

#set new variable pg1trn_cat to 1 if exists
unique(survey$pg1trn)
survey[,pg1trn_cat := 0]
survey[pg1trn == pg1_answers[3], pg1trn_cat := 1]
table(survey$pg1trn_cat)
#capture when question 1 was not completed
survey[pg1submit == "", pg1trn_cat := NA]
table(survey$pg1trn_cat)

#wrangle all the comments into new "other" column
unique(survey$pg1other)
survey[!(pg1trn %in% pg1_answers), pg1other := pg1trn]

#wrangle all original "other" comments into new "other" column
unique(survey$pg1other)
survey[,pg1other_cat := 0]
survey[!(pg1other %in% c(pg1_answers, "Other")), pg1other_cat := 1]
table(survey$pg1other_cat)
#capture when question 1 was not completed
survey[pg1submit == "", pg1other_cat := NA]
table(survey$pg1other_cat)
```

```{r, echo=F} 
#count number of purposes selected
#allowed for multiple selections
survey[,pg1purposes_cat := pg1psnuse_cat + pg1wdauth_cat + pg1trn_cat + pg1other_cat]
#table(survey$pg1purposes_cat)

#all choices
p <- survey[,.(personal_researcher = sum(pg1psnuse_cat, na.rm=T), developer = sum(pg1wdauth_cat, na.rm=T), 
      training = sum(pg1trn_cat, na.rm=T), other = sum(pg1other_cat, na.rm=T))]

show_reasons<- function(p){
  q <- gather(na.omit(p), key = purpose, value = count,
  personal_researcher, developer, training, other) %>%
  ggplot(aes(x = reorder(purpose, -count), y = count)) +
  geom_bar(stat = "identity", fill = "darkturquoise") +
  coord_flip() +
  theme(axis.title.y = element_blank(), plot.title = element_text(hjust = 0.5)) +
  geom_text(aes(label = count))
}

q <- show_reasons(p)
q + ggtitle("Purpose of Project - All Selections")

p <- survey[,.(personal_researcher = sum(pg1psnuse_cat, na.rm=T), developer = sum(pg1wdauth_cat, na.rm=T), 
                training = sum(pg1trn_cat, na.rm=T), other = sum(pg1other_cat, na.rm=T)), by = package]
q <- show_reasons(p)
q + facet_wrap(~package) + ggtitle("Purpose of Project - All Selections")
```

```{r, echo=F}
#1 choice selected
p <- survey[pg1purposes_cat == 1,.(personal_researcher = sum(pg1psnuse_cat, na.rm=T), developer = sum(pg1wdauth_cat, na.rm=T), training = sum(pg1trn_cat, na.rm=T), other = sum(pg1other_cat, na.rm=T))]
q <- show_reasons(p)
q + ggtitle("Purpose of Project - Single Chioce Selected")
```

### Note for respondents who indicated their selection was motivated solely by training, this observation does not represent a choice.

```{r, echo=F}

#2 choices selected
p <- survey[pg1purposes_cat == 2,.(personal_researcher = sum(pg1psnuse_cat, na.rm=T), developer = sum(pg1wdauth_cat, na.rm=T), 
      training = sum(pg1trn_cat, na.rm=T), other = sum(pg1other_cat, na.rm=T))]
q <- show_reasons(p)
q + ggtitle("Purpose of Project - Two Chioces Selected")

#3 choices selected
p <- survey[pg1purposes_cat == 3,.(personal_researcher = sum(pg1psnuse_cat, na.rm=T), developer = sum(pg1wdauth_cat, na.rm=T), 
      training = sum(pg1trn_cat, na.rm=T), other = sum(pg1other_cat, na.rm=T))]
q <- show_reasons(p)
q + ggtitle("Purpose of Project - Three Chioces Selected")
```

### What was included in "Other" purpose for project?

```{r, echo=F, warning=F, eval=T}
other <- survey$pg1other
make_cloud(other)
```

### Question 2 - Does this commit represent the first time that package was introduced into your project?

```{r, echo=F}
#survey$pg2resp <- as.factor(survey$pg2resp)

survey %>%
  filter(pg2resp != "") %>%
  dplyr::group_by(pg2resp, package) %>%
  dplyr::summarize(count = n()) %>%
  ggplot(aes(x=reorder(pg2resp, -count), y=count, group=package, fill=package)) +
  geom_bar(stat="identity", position=position_dodge2(preserve="single")) +
  geom_text(aes(label=count, hjust=0.5, vjust=0),
            position = position_dodge(width = 1))  +
  theme(legend.position = "bottom")+
  labs(title = "Package Introduced for First Time?",x = "Response") +
  theme(plot.title = element_text(hjust = 0.5)) +
  ylim(c(0, 200))
```

### Question 3 - Which of the following is the closest to why you chose that package?  The choices included:

1. The core data.frame object lacked functionality that I needed
2. Chose the package to be compatible with other packages in my project
3. I saw a recommendation for the package
4. I didn't choose to use the package, it was included implicitly / unintentionally
5. Other (please specify)

Note observations where package was unintentionally added to project, does not represent choice.

```{r, echo=F}
pg3_answers <- c("The core \"\"data.frame\"\" object lacked functionality that I needed",
                 "Chose the package to be compatible with other packages in my project",
                 "I saw a recommendation for the package",
                 "I didn't choose to use the package, it was included implicitly / unintentionally",
                 "Other",
                 "")

#set levels for response
survey[pg3resp == pg3_answers[1], pg3reason_cat := "df_functionality"]
survey[pg3resp == pg3_answers[2], pg3reason_cat := "compatibility"]
survey[pg3resp == pg3_answers[3], pg3reason_cat := "recommendation"]
survey[pg3resp == pg3_answers[4], pg3reason_cat := "unintentional"]
survey[!(pg3resp %in% pg3_answers),c("pg3_other", "pg3reason_cat") := list(pg3resp, "other")]
survey[pg3submit == "", pg3reason_cat := NA]
survey$pg3reason_cat <- as.factor(survey$pg3reason_cat)

survey %>%
  filter(!is.na(pg3reason_cat)) %>%
  dplyr::group_by(pg3reason_cat, package) %>%
  dplyr::summarize(count = n()) %>%
  ggplot(aes(x=reorder(pg3reason_cat, -count), y=count,group=package, fill=package)) +
  geom_bar(stat="identity", position=position_dodge2(preserve="single")) +
  geom_text(aes(label=count, hjust=0.5, vjust=0),
            position = position_dodge(width = 1))  +
  theme(legend.position = "bottom") +
  labs(title = "Reason for Selecting Package", x = NULL) +
  theme(plot.title = element_text(hjust = 0.5))
```

### What was included in "Other" reason for data.table?

```{r, echo=F, warning=F, eval=T}
other_data_table <- survey[package == "data.table",pg3_other]
make_cloud(other_data_table)
```

### What was included in "Other"reason for tidy?

```{r, echo=F, warning=F, eval=T}
other_tidy <- survey[package == "tidy",pg3_other]
make_cloud(other_tidy)
```

### Question 4 - How likely is it that you would recommend your package choice to others?

```{r, eval=T, echo=F, warning=F}
survey %>%
  filter(!is.na(package)) %>%
  filter(!is.na(pg4allresp)) %>%
  filter(!(pg4submit == "")) %>%
  dplyr::group_by(pg4allresp, package) %>%
  dplyr::summarize(count = n()) %>%
  ggplot(aes(x=as.factor(pg4allresp), -count, y=count, group=package,
             fill = package)) +
  geom_bar(stat="identity", position=position_dodge2(preserve="single")) +
  geom_text(aes(label=count, hjust=0.5, vjust=0),
            position = position_dodge(width = 1))  +
  theme(legend.position = "bottom")+
  labs(title = "How Likely to Recommend Package",x = "0-Very Unlikely, 10-Very Likely") +
  theme(plot.title = element_text(hjust = 0.5))
```

```{r, eval = T, include = F}
#set new variable pg4_unlikely_cat 6 or less
survey[,pg4_unlikely_cat := 0]
survey[pg4allresp <= 6, pg4_unlikely_cat := 1]
survey[pg4submit == "", pg4_unlikely_cat := NA]
#set new variable pg4_unlikely if 7 or 8
survey[,pg4_neutral_cat := 0]
survey[pg4allresp %in% c(7,8), pg4_neutral_cat := 1]
survey[pg4submit == "", pg4_neutral_cat := NA]
#set new variable pg4_likely if 9 or 10
survey[,pg4_likely_cat := 0]
survey[pg4allresp > 8, pg4_likely_cat := 1]
survey[pg4submit == "", pg4_likely_cat := NA]
```

### For Net Promoter Score Analysis
### 0-6 Detractor, 7-8 Passive, 9-10 Promoter

```{r, echo=F}
p <- survey[,.(Detractor = sum(pg4_unlikely_cat, na.rm=T), Passive = sum(pg4_neutral_cat, na.rm=T), Promoter = sum(pg4_likely_cat, na.rm=T))]

q <- gather(p, key = recommend, value = count,
  Detractor, Passive, Promoter) %>%
  ggplot(aes(x = reorder(recommend, -count), y = count)) +
  geom_bar(stat = "identity", fill = "darkturquoise") +
  coord_flip() +
  theme(axis.title.y = element_blank(), plot.title = element_text(hjust = 0.5)) +
  geom_text(aes(label = count))
  
q + ggtitle("Would You Recommend?")

p <- survey[,.(Detractor = sum(pg4_unlikely_cat, na.rm=T), Passive = sum(pg4_neutral_cat, na.rm=T), 
               Promoter = sum(pg4_likely_cat, na.rm=T)), by = package]

gather(na.omit(p), key = recommend, value = count,
  Detractor, Passive, Promoter) %>%
  ggplot(aes(x = reorder(recommend, -count), y = count)) +
  geom_bar(stat = "identity", fill = "darkturquoise") +
  facet_wrap(~package) +
  coord_flip() +
  theme(axis.title.y = element_blank(), plot.title = element_text(hjust = 0.5)) +
  ggtitle("How Likely Would You Recommend?") +
  geom_text(aes(label = count))
```

### Question 5 - How important were the following factors when chosing that package for your project

```{r, warning=F, eval=T, include=F}
#for question 5 data there are 13 columns of data representing 13 factors that may be important when selecting an R package.  The user could select none, one, some, or all of the factors and drag them to a block indicating the priority of that factor to that user.  The priorities were Essential, High, Medium, Low, or Not.  If the factor was selected and matched with a priority, that priority was recorded in that cell, otherwise the cell was empty.  Some prework was required to analyze this data.
pg5_cols <- c((names(survey))[str_detect(names(survey), "pg5")], "package")
pg5 <- survey[,..pg5_cols]
pg5[pg5submit == "", .N]
pg5 <- pg5[!(pg5submit == ""),] #remove observations that were not submitted
dim(pg5)
table(pg5$package)
typeof(pg5$pg5_1rrpq)

pg5_1 <- as.data.frame(table(pg5$pg5_1rrpq, pg5$package))
pg5_1$choice = "pg5_1rrpq"

pg5_2 <- as.data.frame(table(pg5$pg5_2bnui, pg5$package))
pg5_2$choice = "pg5_2bnui"
pg5_summary <- bind_rows(pg5_1, pg5_2)

pg5_3 <- as.data.frame(table(pg5$pg5_3hds, pg5$package))
pg5_3$choice = "pg5_3hds"
pg5_summary <- bind_rows(pg5_summary, pg5_3)

pg5_4 <- as.data.frame(table(pg5$pg5_4vgp, pg5$package))
pg5_4$choice = "pg5_4vgp"
pg5_summary <- bind_rows(pg5_summary, pg5_4)

pg5_5 <- as.data.frame(table(pg5$pg5_5phr, pg5$package))
pg5_5$choice = "pg5_5phr"
pg5_summary <- bind_rows(pg5_summary, pg5_5)

pg5_6 <- as.data.frame(table(pg5$pg5_6ssyop, pg5$package))
pg5_6$choice = "pg5_6ssyop"
pg5_summary <- bind_rows(pg5_summary, pg5_6)

pg5_7 <- as.data.frame(table(pg5$pg5_7ndyp, pg5$package))
pg5_7$choice = "pg5_7ndyp"
pg5_summary <- bind_rows(pg5_summary, pg5_7)

pg5_8 <- as.data.frame(table(pg5$pg5_8cp, pg5$package))
pg5_8$choice = "pg5_8cp"
pg5_summary <- bind_rows(pg5_summary, pg5_8)

pg5_9 <- as.data.frame(table(pg5$pg5_9frp, pg5$package))
pg5_9$choice = "pg5_9frp"
pg5_summary <- bind_rows(pg5_summary, pg5_9)

pg5_10 <- as.data.frame(table(pg5$pg5_10rpa, pg5$package))
pg5_10$choice = "pg5_10rpa"
pg5_summary <- bind_rows(pg5_summary, pg5_10)

pg5_11 <- as.data.frame(table(pg5$pg5_11nsg, pg5$package))
pg5_11$choice = "pg5_11nsg"
pg5_summary <- bind_rows(pg5_summary, pg5_11)

pg5_12 <- as.data.frame(table(pg5$pg5_12nwg, pg5$package))
pg5_12$choice = "pg5_12nwg"
pg5_summary <- bind_rows(pg5_summary, pg5_12)

pg5_13 <- as.data.frame(table(pg5$pg5_13nfg, pg5$package))
pg5_13$choice = "pg5_13nfg"
pg5_summary <- bind_rows(pg5_summary, pg5_13)

names(pg5_summary) <- c("importance", "package", "freq", "selection")
pg5_summary$importance <- as.factor(pg5_summary$importance)
pg5_summary$package <- as.factor(pg5_summary$package)
pg5_summary$selection <- as.factor(pg5_summary$selection)
sapply(pg5_summary, class)

pg5_summary <- as.data.table(pg5_summary)

#to reorder and rename levels
levels(pg5_summary$importance) <- list(Ess = "Essential", High = "High Priority", Med = "Medium Priority", Low = "Low Priority", Not = "Not a Priority", NotSelected = "")

levels(pg5_summary$selection) <- list(Resolution = "pg5_1rrpq" , Backlog = "pg5_2bnui", StackExchange ="pg5_3hds", Growth = "pg5_4vgp", PkgRep = "pg5_5phr", SizeProject = "pg5_6ssyop", Developers = "pg5_7ndyp", CompPerf = "pg5_8cp", Familiar = "pg5_9frp", AuthRep = "pg5_10rpa", StarsGH = "pg5_11nsg", WatchersGH = "pg5_12nwg", ForksGH = "pg5_13nfg")
```

###13 Factors Presented

1. Resolution - Resolved Reported Problems Quickly
2. Backlog - Backlog/ # of Unresolved Issues
3. StackExchange - Helpful Discussion on StackExchange
4. Growth - Visible Growth in Popularity
5. PkgRep - Package's Historic Reputation
6. SizeProject - Scale/Size of Your Own Project
7. Developers - # Developers of Your Project
8. CompPerf - Computing Performances
9. Familiar - Familiarity with Related Packages
10. AuthRep - Reputation of the Package's Authors
11. StarsGH - # of Stars on GitHub
12. WatchersGH - # of Watchers on GitHub
13. ForksGH - # of Forks on GitHub

```{r, echo=F}
pg5_dt_total <- survey %>%
  filter(package == "data.table") %>%
  filter(!(pg5submit == "")) %>%
  nrow()

pg5_tidy_total <- survey %>%
  filter(package == "tidy") %>%
  filter(!(pg5submit == "")) %>%
  nrow()

pg5_summary %>%
  filter(importance %in% c("Ess", "High")) %>%
  mutate(percent = ifelse(package =="data.table", freq/pg5_dt_total, freq/pg5_tidy_total)) %>%
  ggplot(aes(x = selection, y = percent, fill = package)) +
  geom_bar(position = "dodge", stat = "identity") +
  ggtitle("Percent Essential or High") +
  theme(legend.position = "bottom") +
  theme(axis.title.y = element_blank(), plot.title = element_text(hjust = 0.5)) +
  theme(axis.text.x = element_text(angle = 30, hjust = 1, vjust = 1))
```

### How many factors were selected by each user?

```{r, eval=T, include=F, warning=F}
#gather question 5 columns and isolate order cols
#find lowest col order (should be 1, sometimes not if original first selection overwritten with another selection), and note factor associated with lowest col order
#find highest col order, would be 13 if all cols selected, highest order - lowest order + 1 gives columns selected
pg5_cols <- (names(survey))[str_detect(names(survey), "pg5")]
pg5_cols <- c(pg5_cols, "package")
pg5 <- survey[,..pg5_cols]
pg5[pg5submit == "", .N]
pg5 <- pg5[!(pg5submit == ""),] #remove observations that were not submitted
dim(pg5)
pg5_order_cols <- (names(pg5))[str_detect(names(pg5), "order")]
pg5_order_cols_package <- c(pg5_order_cols, "package")
pg5_order <- pg5[,..pg5_order_cols_package]

find_first <- function(x,y){
  first <- pg5_order_cols[which(y[x,] == 1)]
  if(is_empty(first)) first <- "none"
  first
}

pg5_order[,first_col := vapply(1:nrow(pg5_order), find_first, y=pg5_order, character(1))]
pg5_order[,min_order := sapply(1:nrow(pg5_order), function(x, y) min(y[x,], na.rm=T), y=pg5_order[,1:13])]
pg5_order[,max_order := sapply(1:nrow(pg5_order), function(x, y) max(y[x,], na.rm=T), y=pg5_order[,1:13])]
pg5_order[,num_selected := max_order - min_order + 1]

#number of factors selected
quantile(pg5_order$num_selected, probs = seq(0, 1, 0.1), na.rm = T)
table(pg5_order$num_selected) #-Inf indicates no selection
```

13 factors were displayed, along with 5 boxes labelled with varying degrees of importance.  The user could select any number of factors, dragging them to the box of their assigned importance.  Oddly, some respondents made more than 13 selections.

```{r, echo=F}
pg5_order %>%
  #filter(num_selected > 0) %>%
  ggplot(aes(num_selected)) +
  geom_histogram(fill = "darkturquoise", binwidth = 1) +
  ylim(c(0, 170)) +
  ggtitle("Number of Factors Selected") +
  theme(axis.title.x = element_blank(), plot.title = element_text(hjust = 0.5))+
  geom_text(stat='count', aes(label = ..count..), vjust = -1)
```

### Which factor was selected first?

```{r, echo=F, warning=F}
pg5_order[, first_col_imp := ""]
#head(pg5_order$first_col)
#what was importance of first item selected?
for(i in 1:nrow(pg5_order)){
  pg5_order$first_col_imp[i] <- switch(pg5_order$first_col[i],
                                  pg5_1order = pg5$pg5_1rrpq[i],
                                  pg5_2order = pg5$pg5_2bnui[i],
                                  pg5_3order = pg5$pg5_3hds[i],
                                  pg5_4order = pg5$pg5_4vgp[i],
                                  pg5_5order = pg5$pg5_5phr[i],
                                  pg5_6order = pg5$pg5_6ssyop[i],
                                  pg5_7order = pg5$pg5_7ndyp[i],
                                  pg5_8order = pg5$pg5_8cp[i],
                                  pg5_9order = pg5$pg5_9frp[i],
                                  pg5_10order = pg5$pg5_10rpa[i],
                                  pg5_11order = pg5$pg5_11nsg[i],
                                  pg5_12order = pg5$pg5_12nwg[i],
                                  pg5_13order = pg5$pg5_13nfg[i],
                                  "none"
                                  )
}
pg5_order$importance <- mapply(function(x) ifelse(x %in% c("Essential", "High Priority"), "essential/high priority", "other"), pg5_order$first_col_imp)

pg5_order$first_col <- as.factor(pg5_order$first_col)
pg5_order$importance <- as.factor(pg5_order$importance)

levels(pg5_order$first_col) <- list(Resolution = "pg5_1order" , Backlog = "pg5_2order", StackExchange ="pg5_3order", Growth = "pg5_4order", PkgRep = "pg5_5order", SizeProject = "pg5_6order", Developers = "pg5_7order", CompPerf = "pg5_8order", Familiar = "pg5_9order", AuthRep = "pg5_10order", StarsGH = "pg5_11order", WatchersGH = "pg5_12order", ForksGH = "pg5_13order", None = "none")

pg5_order %>%
  filter(first_col != "None") %>%
  ggplot(aes(first_col, fill = importance)) +
  geom_bar() +
  facet_wrap(~package) +
  #geom_text(stat='count', aes(label=..count..), 
  #          hjust = 0, vjust=0) +
  ylim(0,180) +
  coord_flip() +
  labs(title = "First Factor Selected", x="factor") +
  theme(plot.title = element_text(hjust = 0.5))

```

### Question 6 - Your level of development experience at the time of commit: 

```{r, echo=F}
survey$pg6resp <- as.factor(survey$pg6resp)

#to reorder and rename levels for chart
levels(survey$pg6resp) <- list("<2 years" = "Less than 2 years", "2-5 years" = "2 - 5 years", "6-8 years" = "6 - 8 years", "9-12 years" = "9 - 12 years", "13-19 years" = "13 - 19 years", "20+ years" = "20 years or more")

survey %>%
  filter(!(pg6submit == "")) %>%
  filter(!(is.na(pg6resp))) %>%
  ggplot(aes(pg6resp, fill = package)) +
  geom_bar() +
  theme(axis.text.x = element_text(angle = 30, hjust = 1, vjust = 1)) +
  #ggtitle("Respondent Development Experience") +
  theme(axis.title.y = element_blank(), axis.title.x = element_blank(), plot.title =element_text(hjust = 0.5)) +
  theme(legend.position = "bottom") +
  geom_text(stat='count', position = position_stack(vjust = 0.5), aes(label = ..count..)) 

```

### Question 7 - Your primary language(s) at the time of that commit:

```{r, eval=T, include=F}
languages <- c("R", "C/C++", "Java", "Python", "Javascript", "Go", "C#", "Other", "")

#set new variable pg7r_cat to 1 if R
unique(survey$pg7r)
survey[,pg7r_cat := 0]
survey[pg7r == "R", pg7r_cat := 1]
table(survey$pg7r_cat)
#capture when question 1 was not completed
survey[pg7submit == "", pg7r_cat := NA]
table(survey$pg7r_cat)

#wrangle all the comments into new "other" column
unique(survey$pg7other)
survey[!(pg7r %in% languages), pg7other := pg7r]
unique(survey$pg7other)

#set new variable pg7c_cat to 1 if C/C++
unique(survey$`pg7c/c++`)
survey[,pg7c_cat := 0]
survey[`pg7c/c++` == "C/C++", pg7c_cat := 1]
table(survey$`pg7c_cat`)
#capture when question 1 was not completed
survey[pg7submit == "", pg7c_cat := NA]
table(survey$pg7c_cat)

#wrangle all the comments into new "other" column
survey[!(`pg7c/c++` %in% languages), pg7other := `pg7c/c++`]
unique(survey$pg7other)

#set new variable pg7java_cat to 1 if Java
unique(survey$pg7java)
survey[,pg7java_cat := 0]
survey[pg7java == "Java", pg7java_cat := 1]
table(survey$pg7java_cat)
#capture when question 1 was not completed
survey[pg7submit == "", pg7java_cat := NA]
table(survey$pg7java_cat)

#wrangle all the comments into new "other" column
survey[!(pg7java %in% languages), pg7other := pg7java]
unique(survey$pg7other)

#set new variable pg7python_cat to 1 if Python
unique(survey$pg7python)
survey[,pg7python_cat := 0]
survey[pg7python == "Python", pg7python_cat := 1]
table(survey$pg7python_cat)
#capture when question 1 was not completed
survey[pg7submit == "", pg7python_cat := NA]
table(survey$pg7python_cat)

#wrangle all the comments into new "other" column
survey[!(pg7python %in% languages), pg7other := pg7python]
unique(survey$pg7other)

#set new variable pg7js_cat to 1 if Javascript
unique(survey$pg7javascript)
survey[,pg7js_cat := 0]
survey[pg7javascript == "Javascript", pg7js_cat := 1]
table(survey$pg7js_cat)
#capture when question 1 was not completed
survey[pg7submit == "", pg7js_cat := NA]
table(survey$pg7js_cat)

#wrangle all the comments into new "other" column
survey[!(pg7javascript %in% languages), pg7other := pg7javascript]
unique(survey$pg7other)

#set new variable pg7go_cat to 1 if Go
unique(survey$pg7go)
survey[,pg7go_cat := 0]
survey[pg7go == "Go", pg7go_cat := 1]
table(survey$pg7go_cat)
#capture when question 1 was not completed
survey[pg7submit == "", pg7go_cat := NA]
table(survey$pg7go_cat)

#wrangle all the comments into new "other" column
survey[!(pg7go %in% languages), pg7other := pg7go]
unique(survey$pg7other)

#set new variable pg7cs_cat to 1 if C#
unique(survey$`pg7c#`)
survey[,pg7cs_cat := 0]
survey[`pg7c#` == "C#", pg7cs_cat := 1]
table(survey$pg7cs_cat)
#capture when question 1 was not completed
survey[pg7submit == "", pg7cs_cat := NA]
table(survey$pg7cs_cat)

#wrangle all the comments into new "other" column
survey[!(`pg7c#` %in% languages), pg7other := `pg7c#`]
unique(survey$pg7other)

#set new variable pg7other_cat to 1 if other
survey[,pg7other_cat := 0]
survey[!(pg7other %in% languages), pg7other_cat := 1]
table(survey$pg7other_cat)
#capture when question 1 was not completed
survey[pg7submit == "", p71other_cat := NA]
table(survey$pg7other_cat)

survey[,pg7_count := pg7r_cat + pg7c_cat + pg7java_cat + pg7python_cat + pg7js_cat + pg7go_cat + pg7cs_cat + pg7other_cat]
```

```{r, echo=F}
p <- survey[,.(R = sum(pg7r_cat, na.rm=T), 
               'C/C++' = sum(pg7c_cat, na.rm=T),
               Java = sum(pg7java_cat, na.rm=T), 
               Python = sum(pg7python_cat, na.rm=T),
               JavaScript = sum(pg7js_cat, na.rm=T),
               Go = sum(pg7go_cat, na.rm=T),
               CSharp = sum(pg7cs_cat, na.rm=T),
               Other = sum(pg7other_cat, na.rm=T))]

show_languages<- function(p){
  q <- gather(na.omit(p), key = language, value = count,
  R, 'C/C++', Java, Python, JavaScript, Go, CSharp, Other) %>%
  ggplot(aes(x = reorder(language, -count), y = count)) +
  geom_bar(stat = "identity", fill = "darkturquoise") +
  coord_flip() +
  theme(axis.title.y = element_blank(), plot.title = element_text(hjust = 0.5)) +
  geom_text(aes(label = count))
}

q <- show_languages(p)
q #+ ggtitle("Laguages used by Respondents")

#table(survey$pg7_count)
survey %>%
  filter(pg7submit != "") %>%
  filter(pg7_count > 0) %>%
  ggplot(aes(pg7_count)) +
  geom_histogram(fill = "darkturquoise", binwidth = 1) +
  ylim(c(0, 550)) +
  ggtitle("Number of Languages Selected") +
  theme(axis.title.x = element_blank(), plot.title = element_text(hjust = 0.5))+
  geom_text(stat='count', aes(label = ..count..), vjust = -1) 

```

### "Other" Languages

```{r, echo=F, warning=F, eval=T}
###text mining
make_cloud(survey$pg7other)
```

### Question 8 - At the time of that commit, you were:

Choices given:
1. Software Engineer
2. Data Scientist
3. Other (please specify)

Made an attempt to catagorize "Other" responses.

```{r, warning=F, eval=T, include=F}
#rename levels for categorical variables
survey$pg8resp <- str_trim(tolower(survey$pg8resp))
role_categories <- c("data_sci", "software_eng", "student", "research", "acedemic", "other_eng", "bio", "stat", "")
survey[str_detect(pg8resp, "data scientist"), pg8resp := "data_sci"]
survey[str_detect(pg8resp, "software engineer"), pg8resp := "software_eng"]
survey[str_detect(pg8resp, "student"), pg8resp := "student"]
survey[str_detect(pg8resp, "research"), pg8resp := "research"]
survey[str_detect(pg8resp, "prof"), pg8resp := "acedemic"]
survey[str_detect(pg8resp, "lecture"), pg8resp := "acedemic"]
survey[str_detect(pg8resp, "instruct"), pg8resp := "acedemic"]
survey[str_detect(pg8resp, "ac.demic"), pg8resp := "acedemic"]
survey[str_detect(pg8resp, "faculty"), pg8resp := "acedemic"]
survey[str_detect(pg8resp, "post.*doc"), pg8resp := "acedemic"]
survey[str_detect(pg8resp, "phd"), pg8resp := "acedemic"]
survey[str_detect(pg8resp, "analyst"), pg8resp := "data_sci"]
survey[str_detect(pg8resp, "engineer") & pg8resp != "software_eng", pg8resp := "other_eng"]
survey[str_detect(pg8resp, "bio"), pg8resp := "bio"]
survey[str_detect(pg8resp, "stat"), pg8resp := "stat"]
survey[str_detect(pg8resp, "business intell"), pg8resp := "data_sci"]
```

```{r, echo=F}
other_roles <- survey[!(pg8resp %in% role_categories),pg8resp]

survey[!(pg8resp %in% role_categories), pg8resp := "other"]
survey$pg8resp <- as.factor(survey$pg8resp)

summary <- survey %>%
  filter(pg8submit != "") %>%
  dplyr::group_by(pg8resp, package) %>%
  dplyr::summarize(count = n()) %>%
  arrange(desc(count))
#reorder
summary <- summary %>%
  filter(pg8resp != "") %>%
  dplyr::group_by(pg8resp) %>%
  dplyr::mutate(total = sum(count)) %>%
  transform(pg8resp = reorder(pg8resp, total))
summary %>%
  ggplot(aes(x=pg8resp, y=count, fill = package)) +
  geom_bar(stat = "identity") +
  coord_flip() +
  ggtitle("Roles of Respondents") +
  theme(axis.title.y = element_blank(), plot.title = element_text(hjust = 0.5)) 
```

### "Other" Respondents Roles

```{r, echo=F, warning=F, eval=T}
make_cloud(other_roles)
```

### Question 9 - Until the time of that commit, in how many software projects have you been involved?

```{r, echo=F}
survey$pg9resp <- as.factor(survey$pg9resp)

#to reorder and rename levels
levels(survey$pg9resp) <- list("1" = "1", "2-3" = "2 - 3", "4-6" = "4 - 6", "7-10" = "7 - 10", "11-15" = "11 - 15", "16-25" = "16 - 25", "25+" = "More than 25")

survey %>%
  filter(!(pg9submit == "")) %>%
  filter(!(is.na(pg9resp))) %>%
  ggplot(aes(pg9resp, fill = package)) +
  geom_bar() +
  theme(axis.text.x = element_text(angle = 30, hjust = 1, vjust = 1)) +
  #ggtitle("Respondent Involved in How Many Prior Software Projects?") +
  theme(axis.title.y = element_blank(), axis.title.x = element_blank(), plot.title =element_text(hjust = 0.5)) +
  theme(legend.position = "bottom") +
  geom_text(stat='count', position = position_stack(vjust = 0.5), aes(label = ..count..)) 

```

### Question 10 - How do you rate your English proficiency?

```{r, echo=F}
survey$pg10resp <- as.factor(survey$pg10resp)

#to reorder and rename levels
levels(survey$pg10resp) <- list("Native" = "Native", "Not Native, Full" = "Not Native - full working proficiency" , "Not Native, Sufficienct" = "Not native - sufficient working proficiency", "Not Native, Limited" =  "Not native - limited working proficiency", "Not Native, Passable" = "Not native - passable", "Not Native, Very Limited" = "Not native - very limited")

survey %>%
  filter(!(pg10submit == "")) %>%
  filter(!(is.na(pg10resp))) %>%
  ggplot(aes(pg10resp, fill = package)) +
  geom_bar() +
  theme(axis.text.x = element_text(angle = 30, hjust = 1, vjust = 1)) +
  #ggtitle("Respondent English Proficiency") +
  theme(axis.title.y = element_blank(), axis.title.x = element_blank(), plot.title =element_text(hjust = 0.5)) +
  theme(legend.position = "bottom") +
  geom_text(stat='count', position = position_stack(vjust = 0.5), aes(label = ..count..)) 

```

### Question 11 - Your gender:

```{r, echo=F}
survey$pg11resp <- as.factor(survey$pg11resp)

survey %>%
  filter(!(pg11submit == "")) %>%
  filter(pg11resp %in% c("Female", "Male", "Prefer not to answer")) %>%
  ggplot(aes(pg11resp, fill = package)) +
  geom_bar() +
  theme(axis.text.x = element_text(angle = 30, hjust = 1, vjust = 1)) +
  #ggtitle("Respondent Gender") +
  theme(axis.title.y = element_blank(), axis.title.x = element_blank(), plot.title =element_text(hjust = 0.5)) +
  theme(legend.position = "bottom") +
  geom_text(stat='count', position = position_stack(vjust = 0.5), aes(label = ..count..))

```

### Question 12 - Your Age:

```{r, echo=F}
survey$pg12resp <- as.factor(survey$pg12resp)

survey %>%
  filter(!(pg12submit == "")) %>%
  filter(!(is.na(pg12resp))) %>%
  ggplot(aes(pg12resp, fill = package)) +
  geom_bar() +
  theme(axis.text.x = element_text(angle = 30, hjust = 1, vjust = 1)) +
  #ggtitle("Respondent Age") +
  theme(axis.title.y = element_blank(), axis.title.x = element_blank(), plot.title =element_text(hjust = 0.5)) +
  theme(legend.position = "bottom") +
  geom_text(stat='count', position = position_stack(vjust = 0.5), aes(label = ..count..))

```
