ver=N
for LA in C Cs F Go JS PY R Rust Scala java jl pl rb
do ls -f $ver$LA.* | sed "s|$ver$LA\.||" | while read i; do zcat $ver$LA.$i|cut -d\; -f2 | sort | uniq -c | awk '{print "'$i';"$2";"$1}'; done > cnt.$ver$LA
done

png(filename="R-modules.png",width=900,height=650);
nn=gsub("NR.","",system("ls NR.*",intern=T));
N=length(nn);
first = 1;
for (m in nn){
    x = read.table(paste("NR",m,sep="."), sep=";");
    x=x[x[,2]>2012,];
    x=x[x[,2]<=2018,];
    cnt=table(x[,2]);
    cnt=cnt[-length(cnt)];
    labs=names(cnt);
    cnt=as.double(cnt);
    cnt[cnt < 10]=NA;
    if (first==1){
        plot (labs, cnt, ylim=c(10,5000),main="Module trends for R",
              xlab="Years", ylab="New projects per month",
              type="o", lwd=2, lty=1,col=1,log="y");
    }else{
        lines(labs, cnt,col=first,lty=first,lwd=2);
    }
    first = first +1;
    print (max(cnt));
}
legend(2017,400,legend=nn,lty=1:N,col=1:N,lwd=2)
dev.off();


for (LA in c("F", "jl", "rb")){

for (LA in c("C", "Cs", "Go", "JS", "PY", "Rust", "Scala", "java", "pl")){


y = read.table(paste("cnt.N",LA,sep=""), sep=";",col.names=c("m","y","c"));
ll = -sort (-tapply(y$c,y$m,max))

for (prt in 0:2){
  png(filename=paste(c(LA,"-modules.",prt,".png"), collapse=""),width=900,height=650);
  rng = (1:10)+prt*10;
  first = 1;
  N=10;
  for (m in names(ll)[rng]){
    x = y[y$m==m,];
    x=x[x$y>2012&x$y<2019,];
    x=x[-dim(x)[1],];
    x$c[x$c < 10]=NA;
    if (first==1){
      mx = max(x$c,na.rm=T);
      mn = mx/100;
      if (mn <100) mn = 100;
      plot (x$y, x$c, ylim=c(mn,mx),main=paste("Module trends for", LA),
              xlab="Years", ylab="New projects per month",
              type="o", lwd=2, lty=1,col=1,log="y");
    }else{
        lines(x$y, x$c,col=first,lty=first,lwd=2);
    }
    first = first +1;
  }
  legend(2017.1,min(mn*3,mx-mn),legend=names(ll)[rng],lty=1:N,col=1:N,lwd=2,bg="white")
  dev.off();
}  

}


